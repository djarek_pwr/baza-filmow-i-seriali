from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser


class Film(models.Model):
    title = models.CharField(max_length=255)
    director = models.CharField(max_length=255)
    average_rating = models.IntegerField()
    release_date = models.DateField()
    def __str__(self):
        return self.title


class Genre(models.Model):
    name = models.CharField(max_length=255)
    form = models.CharField(max_length=255)
    time_frame = models.CharField(max_length=255)


class Review(models.Model):
    reviewer = models.ForeignKey(settings.AUTH_USER_MODEL)
    rating = models.IntegerField()
    film = models.ForeignKey(Film)

#class User(AbstractBaseUser):
#    user_name = models.CharField(max_length=15, unique=True)
#    first_name = models.CharField(max_length=255)
#    second_name = models.CharField(max_length=255)
#    USERNAME_FIELD = 'user_name'
#    is_active = models.BooleanField(default=True)
#    address = models.CharField(max_length=255)

STATES = (
    (0, 'Available'),
    (1, 'Reserved'),
    (2, 'Rented')
)


class Copy(models.Model):
    state = models.IntegerField(max_length=1, choices=STATES)
    film = models.ForeignKey(Film)


class RentStatus(models.Model):
    copy = models.ForeignKey(Copy)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    returned = models.BooleanField(default=False)

    class Meta:
        app_label = 'baza_filmow_i_seriali'