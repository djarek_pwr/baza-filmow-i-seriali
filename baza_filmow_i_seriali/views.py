from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseBadRequest
from django.db.models import Avg
from .models import *


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class FilmPageList(TemplateView):
    template_name = "films.html"

    def get_context_data(self, **kwargs):
        context = super(FilmPageList, self).get_context_data(**kwargs)

        #copies = Copy.objects.filter(state=0)
        #context['films'] = {copy.film for copy in copies}
        context['films'] = Film.objects.all()
        return context

class FilmDetailsView(TemplateView):
    template_name = "film_details.html"
    
    def get_context_data(self, **kwargs):
        context = super(FilmDetailsView, self).get_context_data(**kwargs)
        found_film = get_object_or_404(Film, id=kwargs['id'])
        context['film'] = found_film
        found_reviews = Review.objects.filter(film=found_film)
        context['reviews'] = found_reviews        
        context['average_rating'] = found_reviews.aggregate(Avg('rating'))['rating__avg']
        if self.request.user.is_authenticated():
            try:
                context['user_rating'] = Review.objects.get(film=found_film, reviewer=self.request.user)
            except Review.DoesNotExist:
                pass

        found_copies = Copy.objects.filter(film=found_film, state=2)
        found_rent_status = RentStatus.objects.filter(copy__in=found_copies, returned=False)
        if found_rent_status:
            context['already_rented'] = True
        else:
            try:
                context['copy'] = Copy.objects.filter(film=found_film, state=0)[0]
            except IndexError:
                pass
        return context

class RentStatusPageList(LoginRequiredMixin, TemplateView):
    template_name = "rent_status_list.html"

    def get_context_data(self, **kwargs):
        context = super(RentStatusPageList, self).get_context_data(**kwargs)
        context['rent_status_list'] = RentStatus.objects.filter(user=self.request.user).order_by('returned')
        return context


class ReviewsList(LoginRequiredMixin, TemplateView):
    template_name = "reviews.html"

    def get_context_data(self, **kwargs):
        context = super(ReviewsList, self).get_context_data(**kwargs)
        context['reviews'] = Review.objects.filter(reviewer=self.request.user).order_by('rating')
        return context


class HomePageView(TemplateView):
    template_name = "index.html"


class RentFilmView(LoginRequiredMixin, TemplateView):
    template_name = "rent.html"
    
    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        found_film = get_object_or_404(Film, id=kwargs['id'])
        found_copy = get_object_or_404(Copy, id=kwargs['copy_id'], film=found_film)
        found_rent_status = RentStatus.objects.filter(copy=found_copy, returned=False)
        if found_rent_status:
            return HttpResponseBadRequest()
        found_copy.state = 2
        found_copy.save()

        status = RentStatus(user=request.user, copy=found_copy)
        status.save()
        return redirect("films")

    def get_context_data(self, **kwargs):
        context = super(RentFilmView, self).get_context_data(**kwargs)
        found_film = get_object_or_404(Film, id=kwargs['id'])
        context['film'] = found_film
        context['copies'] = Copy.objects.filter(film=found_film, state=0)
        return context;

class AddreviewView(LoginRequiredMixin, TemplateView):
    template_name = "addreview.html"
    
    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        found_film = get_object_or_404(Film, id=kwargs['id'])
        ratvalue = request.POST['rate']
        found_reviews = Review.objects.filter(reviewer=request.user, film=found_film)

        if found_reviews:
            return redirect("films")
        rev = Review.objects.create(reviewer=request.user, rating=ratvalue, film=found_film)
        rev.save()
        return redirect("films")

    def get_context_data(self, **kwargs):
        context = super(AddreviewView, self).get_context_data(**kwargs)
        found_film = get_object_or_404(Film, id=kwargs['id'])
        context['film'] = found_film        
        return context;

class LoginView(TemplateView):
    template_name = "login.html"
    success_template = "index.html"
    
    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user and user.is_active:
            login(request, user)
            return render(request, self.success_template, context)
        else:
            context['login_failed'] = True
            return render(request, self.template_name, context)


class LogoutView(LoginRequiredMixin, TemplateView):
    template_name = "logout.html"
    
    def get(self, request, **kwargs):
        logout(request)
        return super(LogoutView, self).get(self, request, **kwargs)