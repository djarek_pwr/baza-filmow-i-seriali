from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
admin.autodiscover()
from .views import *

urlpatterns = patterns('',
    # Examples:
    # 
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^films/rent/(?P<id>[0-9]+)/copy/(?P<copy_id>[0-9]+)$', RentFilmView.as_view(), name='films_rent'),
    url(r'^films/rent/(?P<id>[0-9]+)$', RentFilmView.as_view(), name='films_rent'),
    url(r'^films/$', FilmPageList.as_view(), name='films'),
    url(r'^films/details/(?P<id>[0-9]+)$', FilmDetailsView.as_view(), name='film_details'),
    url(r'^user/rented_films$', RentStatusPageList.as_view(), name='rent_status_list'),
    url(r'^films/addreview/(?P<id>[0-9]+)$', AddreviewView.as_view(), name='films_addreview'),
    url(r'^reviews/$', ReviewsList.as_view(), name='reviews'),
    url(r'^login/$', LoginView.as_view()),
    url(r'^logout/$', LogoutView.as_view()),
    url(r'^admin/', include(admin.site.urls)),
)
