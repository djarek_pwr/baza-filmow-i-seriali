from django.contrib import admin
from baza_filmow_i_seriali.models import Film, Copy, Genre, Review

admin.site.register(Film)
admin.site.register(Copy)
admin.site.register(Genre)
admin.site.register(Review)
#admin.site.register(User)